FROM node:slim
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 7000
CMD ["node", "app.js"]
