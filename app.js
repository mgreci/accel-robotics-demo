const express = require('express');
const app = express();
const Sensor = require('./sensor.js');

const PORT = 80;

app.listen(PORT, () => {
	console.log(`Sensor server is listening on ${PORT}`);
});

app.use(express.static('public'));

app.get('/sensor', (req, res) => {
	res.json({
		data: Sensor.readValue()
	});
});