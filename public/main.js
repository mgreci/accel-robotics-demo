const sensorValueElement = document.getElementById('value');
if (sensorValueElement == null) {
	console.warn('Unable to find sensor element. Exiting');
}

const updateSensorValue = () => {
	fetch('/sensor')
		.then(res => res.json())
		.then(result => {
			//verify result is correct form
			if (!result.hasOwnProperty('data')) {
				console.warn("No data property from /sensor endpoint");
				return;
			}
			
			//write result to the view
			sensorValueElement.innerText = result.data;
		})
		.catch(error => {
			console.error("Error with sensor API: ", error);
		});
};

//update sensor value every 1000 ms
setInterval(updateSensorValue, 1000);