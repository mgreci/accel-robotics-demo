const Sensor = require('../sensor.js');

describe('Read from sensor', () => {
	it('should be between 0 and 1', () => {
		const result = Sensor.readValue();
		expect(result).toBeGreaterThanOrEqual(0);
		expect(result).toBeLessThanOrEqual(1);
	});
});